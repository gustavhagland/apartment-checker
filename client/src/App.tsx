import axios, { AxiosResponse } from "axios"
import { createSignal, For, onMount, Show } from "solid-js"
import { ApartmentAd } from "../../lib/ApiTypes"
import ApartmentList from "./ApartmentList"

const url = import.meta.env.DEV
    ? "http://localhost:8000"
    : ""

const statusOrderMap: Record<
    ApartmentAd["status"],
    number
> = {
    NEW: -1,
    INTERESTING: 0,
    BORING: 1
}

const statusColorMap: Record<
    ApartmentAd["status"],
    "blue" | "green" | "red"
> = {
    NEW: "blue",
    BORING: "red",
    INTERESTING: "green"
}

const App = () => {
    const [data, setData] = createSignal<ApartmentAd[]>()

    const adComparator = (a: ApartmentAd, b: ApartmentAd) =>
        statusOrderMap[a.status] - statusOrderMap[b.status]

    onMount(async () => {
        const response = await axios.get<ApartmentAd[]>(
            url + "/data"
        )
        setData(response.data.sort(adComparator))
    })

    const updateWithStatus = async (ad: ApartmentAd) => {
        const response = await axios.put<
            ApartmentAd,
            AxiosResponse<ApartmentAd[]>
        >(url + "/rate", ad)
        setData(response.data.sort(adComparator))
    }

    return (
        <>
            <header>
                <h1 class="text-9xl font-semibold text-center p-6">
                    {data()?.length ?? 0}
                </h1>
            </header>
            <main>
                <Show when={data()}>
                    {(apartments) => (
                        <ApartmentList
                            apartments={apartments}
                            onUpdate={updateWithStatus}
                        />
                    )}
                </Show>
            </main>
        </>
    )
}

export default App
