export type ApiApartmentAd = {
    AnnonsId: number
    Ungdom: boolean
    Gatuadress: string
    Hyra: number
    Stadsdel: string
    Yta: number
    AntalRum: number
    Vaning: number
    AnnonseradTill: string
    Balkong: boolean
    Korttid: boolean
}

export type ApartmentAd = ApiApartmentAd & {
    status: "NEW" | "INTERESTING" | "BORING"
}
