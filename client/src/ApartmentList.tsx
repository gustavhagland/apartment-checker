import { Icon } from "solid-heroicons"
import { For, Match, Show, Switch } from "solid-js"
import { ApartmentAd } from "../../lib/ApiTypes"
import { eye, moon } from "solid-heroicons/solid"

type Props = {
    apartments: ApartmentAd[]
    onUpdate: (apartment: ApartmentAd) => void
}

const statusColorMap: Record<
    ApartmentAd["status"],
    string
> = {
    NEW: "bg-white",
    BORING: "bg-gray-300",
    INTERESTING: "bg-cyan-200"
}

const ApartmentList = (props: Props) => {
    return (
        <ul class="p-2 container flex-grow mx-auto">
            <For each={props.apartments}>
                {(ad) => (
                    <li
                        class={`p-4 my-7 shadow-lg rounded-xl flex-col justify-between`}
                    >
                        <div class="flex justify-between">
                            <h4 class="text-xl">
                                {ad.Gatuadress}
                            </h4>
                            <a
                                class="underline text-blue-700"
                                href={`https://bostad.stockholm.se/Lista/Details?aid=${ad.AnnonsId}`}
                            >
                                See detaljer
                            </a>
                        </div>
                        <h5>{ad.Stadsdel}</h5>
                        <div class="flex flex-start space-x-2">
                            <p class="inline">
                                {ad.Hyra} kr/mån
                            </p>
                            <p>{ad.AntalRum} rum</p>
                            <p>{ad.Yta} kvm</p>
                            <p>
                                Våning:{" "}
                                {ad.Vaning === 0
                                    ? "BV"
                                    : ad.Vaning}
                            </p>
                            <p>
                                Anmälningsbar tills:{" "}
                                {ad.AnnonseradTill}
                            </p>
                            <Show when={ad.Balkong}>
                                <p>Har balkong</p>
                            </Show>
                            <Switch>
                                <Match when={ad.Korttid}>
                                    <p>Korttids</p>
                                </Match>
                                <Match when={!ad.Korttid}>
                                    <p>Tillsvidare</p>
                                </Match>
                            </Switch>
                        </div>
                        <div class="flex justify-end space-x-3">
                            <Show
                                when={ad.status === "NEW"}
                            >
                                <button
                                    onClick={() =>
                                        props.onUpdate({
                                            ...ad,
                                            status: "INTERESTING"
                                        })
                                    }
                                    class="bg-cyan-300 hover:bg-cyan-400 active:bg-cyan-500 rounded-md px-3 py-1"
                                >
                                    Intressant
                                </button>
                                <button
                                    onClick={() =>
                                        props.onUpdate({
                                            ...ad,
                                            status: "BORING"
                                        })
                                    }
                                    class="bg-gray-300 hover:bg-gray-400 active:bg-gray-500 rounded-md py-1 px-3"
                                >
                                    Ointressant
                                </button>
                            </Show>
                            <Show
                                when={
                                    ad.status ===
                                    "INTERESTING"
                                }
                            >
                                <p>Intressant</p>
                                <button
                                    class="p-1 bg-gray-300 hover:bg-gray-400 active:bg-gray-500 rounded-md"
                                    onClick={() =>
                                        props.onUpdate({
                                            ...ad,
                                            status: "BORING"
                                        })
                                    }
                                >
                                    <Icon
                                        path={moon}
                                        width={20}
                                        height={20}
                                    />
                                </button>
                            </Show>
                            <Show
                                when={
                                    ad.status === "BORING"
                                }
                            >
                                <p>Snark</p>
                                <button
                                    class="py-1 pl-1 bg-cyan-300 hover:bg-cyan-400 active:bg-cyan-500 rounded-md flex space-x-1 pr-2"
                                    onClick={() =>
                                        props.onUpdate({
                                            ...ad,
                                            status: "INTERESTING"
                                        })
                                    }
                                >
                                    <Icon
                                        path={eye}
                                        width={20}
                                        height={20}
                                    />
                                    <p>Intressant</p>
                                </button>
                            </Show>
                        </div>
                    </li>
                )}
            </For>
        </ul>
    )
}

export default ApartmentList
