import axios from "axios"
import express, { json, Request } from "express"
import cors from "cors"
import wu from "wu"

import {
    ApartmentAd,
    ApiApartmentAd
} from "../../lib/ApiTypes"
import { readFile, writeFile } from "fs/promises"

const dataPath = "data.json"

const getStoredData = async (): Promise<
    Map<number, ApartmentAd>
> => {
    try {
        const file = await readFile(dataPath, {
            encoding: "utf-8"
        })
        return new Map(
            Object.entries(
                JSON.parse(file) as Record<
                    string,
                    ApartmentAd
                >
            ).map(([key, value]): [number, ApartmentAd] => [
                parseInt(key),
                value
            ])
        )
    } catch (err) {
        console.warn(
            "Could not read stored user data: ",
            err,
            "Starting with empty data"
        )
        return new Map()
    }
}

const writeData = (data: Map<number, ApartmentAd>) => {
    writeFile(
        dataPath,
        JSON.stringify(Object.fromEntries(data.entries())),
        {
            encoding: "utf-8"
        }
    )
}

let ads = await getStoredData()

const app = express()

app.use(
    cors({
        origin: "http://localhost:3000"
    })
)

app.use(json())

app.get("/data", (_, res) => res.json([...ads.values()]))

app.get("/hello", (_, res) => res.send("Hello"))

app.put("/rate", (req, res) => {
    console.log("Rating ad")
    const existing = ads.get(req.body?.AnnonsId)
    if (existing && req.body.status) {
        ads.set(req.body?.AnnonsId, {
            ...existing,
            status: req.body.status
        })
        writeData(ads)
        return res.status(200).json([...ads.values()])
    }
    return res.sendStatus(400)
})

app.use(express.static("./client/dist"))

const updateData = async () => {
    console.log("Updating data...")
    const fetchedAds = new Map(
        (
            await axios.get<ApiApartmentAd[]>(
                "https://bostad.stockholm.se/Lista/AllaAnnonser"
            )
        ).data
            .filter((ad) => ad.Ungdom)
            .filter((ad) => ad.AntalRum < 3)
            .map((ad) => [ad.AnnonsId, ad])
    )

    //Get all new ads
    const newAds = wu(fetchedAds.entries())
        .filter(([key]) => !ads.has(key))
        .map(([key, value]): [number, ApartmentAd] => [
            key,
            { ...value, status: "NEW" }
        ])

    //Remove all stale ads
    const filteredOldAds = wu(ads.entries()).filter(
        ([key]) => fetchedAds.has(key)
    )

    ads = new Map([...filteredOldAds, ...newAds])

    writeData(ads)

    setTimeout(updateData, 10000)
}

app.listen(8000, () => {
    console.log("Listening on port 8000")
    updateData()
})
